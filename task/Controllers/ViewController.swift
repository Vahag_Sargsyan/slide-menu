//
//  ViewController.swift
//  task
//
//  Created by Creative Company on 5/6/19.
//  Copyright © 2019 Creative Company. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var lbl = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: NSNotification.Name(rawValue: "didSelect"), object: nil)
        let mainVC = UIViewController()
        mainVC.view.backgroundColor = .white
        let rootController = RootViewController(mainViewController: mainVC, topNavigationLeftImage: UIImage(named: "menu-icon"))
        let menuVC = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        let drawerVC = DrawerController(rootViewController: rootController, menuController: menuVC)
        lbl = UILabel.init(frame: drawerVC.view.frame)
        lbl.center = drawerVC.view.center
        lbl.font = UIFont.boldSystemFont(ofSize: 40)
        lbl.textAlignment = NSTextAlignment.center
        mainVC.view.addSubview(lbl)
        
        self.addChild(drawerVC)
        view.addSubview(drawerVC.view)
        drawerVC.didMove(toParent: self)
       
    }
    
    @objc func onDidReceiveData(_ notification: Notification){
        lbl.text = stringFromAny(notification.object)
    }
    
    func stringFromAny(_ value:Any?) -> String {
        if let nonNil = value, !(nonNil is NSNull) {
            return String(describing: nonNil)
        }
        return ""
    }
}
