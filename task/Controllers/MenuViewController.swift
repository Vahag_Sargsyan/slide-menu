//
//  MenuViewController.swift
//  task
//
//  Created by Creative Company on 5/6/19.
//  Copyright © 2019 Creative Company. All rights reserved.
//

import UIKit



class MenuViewController: UIViewController , UITableViewDataSource, UITableViewDelegate{
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = String(indexPath.row) + "  Пункт меню "
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nc = NotificationCenter.default
        nc.post(name: Notification.Name("didSelect"), object: indexPath.row)
        
    }
    

}
